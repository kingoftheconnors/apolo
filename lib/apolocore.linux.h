
struct native_pipe_info
{
    int write_fd;
    int error_fd;
    int input_fd;
    int eval_pipe_fd;
    int err_pipe_fd;
};